\documentclass[11pt]{beamer}
\usetheme{Berkeley}
\usepackage[utf8]{inputenc}
\usepackage[french]{babel}
\usepackage[T1]{fontenc}
\usepackage{amsmath}
\usepackage{amsfonts}
\usepackage{amssymb}
\usepackage{graphicx}
\usepackage{hyperref}
\author{Justin Cano}
\title{Localisation de robots}
%\setbeamercovered{transparent} 
%\setbeamertemplate{navigation symbols}{} 
%\logo{} 
\institute{
École Centrale de Marseille} 
\date{10 janvier 2020} 

\newcommand{\mbf}{\mathbf}

\AtBeginSection[]
{
  \begin{frame}<beamer>
    \frametitle{Plan de la section \thesection}
    \tableofcontents[currentsection]
  \end{frame}
}

\addtobeamertemplate{navigation symbols}{}{%
    \usebeamerfont{footline}%
    \usebeamercolor[fg]{footline}%
    \hspace{1em}%
    \insertframenumber/\inserttotalframenumber
}

%\subject{} 
\begin{document}

\begin{frame}
\titlepage
\end{frame}

\begin{frame}[allowframebreaks]{Arnaud et Justin}
\begin{exampleblock}{Arnaud Venet}
\begin{itemize}
\item Promo entrante 2015;
\item M.S. Polymtl (Génie Électrique), Centrale (\texttt{Still proceeding...});
\item Expertise: \textbf{localisation et cartographie simultanée (SLAM), vision, apprentissage} ;
\item Membre de \textit{Air Gab One} (Drones au sein de E-Gab) en 2016.
\end{itemize}
\end{exampleblock}

\framebreak
\begin{alertblock}{Justin Cano}
\begin{itemize}
\item Promo entrante 2014;
\item M.S. Polytechnique Montréal (Génie Électrique), Centrale (juste là);
\item Expertise: \textbf{fusion de senseurs, SWARMS, capteurs UWB, automatique};
\item Président de E-Gab en 2015-16.
\end{itemize}
\end{alertblock}

\textbf{Nota bene :} Ces slides sont disponible sur mon site perso \texttt{justincano.com}
\end{frame}



\begin{frame}
\setcounter{tocdepth}{1}
\tableofcontents
\setcounter{tocdepth}{3}
\end{frame}
\section[Localisation]{Le problème de localisation}
\subsection{Formulation}
\begin{frame}{Problème}
\begin{itemize}
\item Comment localiser un robot dans un espace balisé?
\pause
\end{itemize}
\begin{alertblock}{Définition}
Localiser c'est estimer la \textbf{position} d'un objet. \\
En \textbf{2D} c'est estimer la coordonnée $(x,y)$.\\
On appellera le vecteur $\mbf p$ le \textbf{vecteur de position} :
\[ \mbf p = \begin{bmatrix}
x \\
y 
\end{bmatrix} .\]
\end{alertblock}
\pause 
La question est maintenant de savoir de \textbf{quelles données disposons-nous} pour résoudre le problème et trouver un \textbf{algorithme de résolution}.
\end{frame}
\subsection[Cinématique]{Cinématique d'un robot 2D (UGV)}
\begin{frame}[allowframebreaks]{Modèle cinématique d'un robot 2D}
\begin{block}{Contraintes}
\begin{itemize}
\item Un robot 2D évolue dans un plan $\mathbb{R}^2$;
\item Il ne peut se déplacer transversalement $\mbf v . \mbf e_y = 0$.
\end{itemize}
\end{block}

\begin{figure}
\centering
\includegraphics[width=0.7\columnwidth]{fig/robot_differentiel.pdf}
\caption{Robot vu de dessus.}
\end{figure}

\begin{block}{Question}
Quelles variables d'état suffisent à décrire l'état cinématique d'un robot?
\end{block}

\framebreak

\begin{itemize}
\item Sa \textbf{position} $\mbf p = [x,y]$;
\item Son \textbf{attitude} (orientation) $\theta$;
\item Sa \textbf{vitesse de rotation} $\omega = \dot{\theta}$;
\item Sa \textbf{vitesse de translation} (ou tangentielle) $v_t = \sqrt{\dot{x}^2 + \dot{y}^2}$;
\end{itemize}
L'état est le suivant :
\[ \mbf x = \begin{bmatrix}
x & y & v_t & \theta & \omega
\end{bmatrix}^\top ,\]
Les \textbf{équations cinématiques} d'un \emph{robot 2D avec contrainte non-holonome} (roulement sans glissement) est la suivante :
\[ \dot{x} = v_t \cos(\theta), \quad  \dot{y} = v_t \sin(\theta), \quad \dot{\theta}=\omega. \]
\end{frame}

\section[Capteurs]{Types de mesures}
\subsection{\textit{Dead Reckoning}}

\begin{frame}[allowframebreaks]{\textit{Dead reckoning}}
Un capteur en \textbf{dead reckoning} (DR) est un capteur estimant des états cinématiques internes du robot.
\begin{exampleblock}{Exemples}
\begin{itemize}
\item Un \textbf{accéléromètre} mesure l'accélération $\ddot{\mbf p}_{R/0} = {\mbf a}_{R/0}$;
\item Un \textbf{gyroscope} mesure la vitesse angulaire $\mbf \omega_{R/0}$;
\item Les \textbf{encodeurs} (odométrie) mesurent la vitesse des roues $\omega_g,  \omega_d$.
\end{itemize}

\end{exampleblock}
Le souci du \textbf{DR} est l'intégration des erreurs de mesures. Il ne faut surtout pas l'utiliser de manière solitaire... il faut des mesures externe.

\framebreak
\begin{block}{Exemple : localisation à l'accéléromètre}
Soit un robot décrivant le cercle unitaire. On suppose qu'il ne dispose que d'un accéléromètre sujet à un bruit blanc gaussien $\mbf n$ :
\[ \tilde{\mbf a} = \mbf a + \mbf n, \mbf n \sim \mathcal{N}(\mbf b,\Sigma)\]
avec $ \mbf b = 10^{-3} \mathrm{m.s^{-2}}$ et $\Sigma = (10^{-2})^2 \mbf I_2 ~\mathrm{[m.s^{-2}]^2}$.
\end{block}

On peut construire un estimé de position $\hat{\mbf p}$ en intégrant l'équation cinématique $\ddot{\mbf p} = \mbf a$  deux fois :
\[ \hat{\mbf p}(t) = \int_{\sigma=0}^t \int_{\xi = 0}^\sigma \tilde{\mbf a}(\xi) d\sigma d\xi \]
\textbf{Cas de figure Bisounours :} on se donne la position et la vitesse initiales $\hat{\mbf p}_0 = \mbf p_0$ et $\hat{\dot{\mbf p}}_0 = \dot{\mbf p}_0$.

\begin{figure}
\centering
\includegraphics[width=0.5\textwidth]{fig/dr.eps}
\caption{Résultat des simulations}
\end{figure}

\begin{itemize}
\item Nous nous devons de corriger cela! 
\item L'erreur provient du fait que \textbf{toutes les mesures sont internes}.
\item [$\Rightarrow$] Nécessité d'introduire des mesures externes pour corriger \textbf{la dérive} (\textit{drift}) d'intégration.
\end{itemize}

\end{frame}

\subsection{\textit{Position Fixing}}

\begin{frame}{\textit{Position Fixing}}
Un capteur donnant des informations de \textbf{position fixing} (PF) est un capteur estimant une grandeur relative entre \textbf{le référentiel du robot} et le \textbf{référentiel global}.

\begin{exampleblock}{Exemples}
\begin{itemize}
\item Le \textbf{GPS} (donne un estimé de position);
\item Une \textbf{caméra} (peut donner des positions relatives);
\item Des \textbf{Balises \textit{Ultra-Wide-Band} (UWB)} (donnent des distances entre des positions connues et le robot).
\end{itemize}
\end{exampleblock}
Le souci du PF est la \textbf{fréquence de rafraîchissement}. Mais combiné au DR il est redoutable et c'est ce qu'on va voir... 
\end{frame}


\section[Moindres carrés]{Moindres carrés (LS, \textit{Least Squares})}

\subsection[Formulation]{Formulation du problème de multilatération}

\begin{frame}[allowframebreaks]{Multilatération et moindres carrés}
Supposons que nous disposons d'un capteur PF nous donnant des rayons $\tilde{\rho}$  bruités entre le robot $E$ de position $\mbf p$ et de $N$ ancres (stations) $A_i$ à positions connues $\mbf a_i$ :
\[ \tilde{\rho}_i = ||\mbf a_i - \mbf p||_2 + \nu, ~ \nu \sim \mathcal{N}(0,\sigma^2) \] 
\begin{center}
\includegraphics[width=0.5\textwidth]{fig/toa_radius.pdf}
\end{center}
On connaît la relation entre la vraie position $\mbf p$ et la mesure $\mbf \rho_i$ en l'absence de bruit (sans le tilde).
\[ \mbf \rho_i = ||\mbf p - \mbf a_i||_2 = f_i(\mbf p) . \]
Ceci est intéressant car pour trouver l'estimé $\hat{\mbf p}$ de $\mbf p$ qui colle le mieux il suffit de minimiser le résidu :
\[ J(\mbf p) = \frac{1}{2}\sum_{i=0}^{N-1} [f_i(\mbf p)-\tilde{\rho}_i]^2 \]
on peut utiliser $\tilde{\rho}_i$ car le bruit $\nu$ est supposé non-biaisé (à moyenne nulle).
\textbf{Cette méthode s'appelle la méthode de moindre carrés}, et notre estimé vaut dans ce cas $\hat{\mbf p} = \mathrm{argmin}_{\mbf p} J(\mbf p)$ c'est-à-dire que $\min(J(\mbf p)) = J(\hat{\mbf p})$.
\end{frame}


\subsection[Gauss-Newton]{Méthode de Gauss-Newton}

\begin{frame}{Résolution : méthode de Gauss-Newton}
On peut utiliser une \textbf{descente de gradient} pour résoudre le problème. On se donne un estimé initial $\hat{\mbf p}_0$ et on applique la formule de récurrence suivante :
\[ \hat{\mbf p}_{i} = \hat{\mbf p}_{i-1} + (\mbf D_r^\top \mbf D_r)^{-1}\mbf D_r^\top \mbf r,\]
\begin{itemize}
\item [$p_i$] sont les estimés à l'étape $i$ on s'arrête lorsque $||\hat{\mbf p}_i - \hat{\mbf p}_{i-1}||<\epsilon$ qui est un seuil à fixer.
\item [$\mbf r$] est le vecteur des résidus ayant pour composantes $r_i = f_i(\hat{\mbf p}) - \tilde{\rho}_i$.
\item [$\mbf D_r$] est la jacobienne $\frac{\partial \mbf r}{\partial p}$ (matrice "2 lignes" ($2\times N$) une pour x, une pour y).
\end{itemize}
\end{frame}

\begin{frame}{Discussion de la pertinence des Moindres Carrés (LS)}
\begin{exampleblock}{Avantages}
\begin{itemize}
\item Robuste au bruit;
\item Implémentations facilement trouvables sur tous les langages (cherchez sur les internets \textit{least-squares} ;) ) sur Matlab fonction \texttt{lsqnonlin()}.
\end{itemize}

\end{exampleblock}

\begin{alertblock}{Inconvénients}
\begin{itemize}
\item Ne prennent pas en compte la dynamique et les mesures de DR;
\item On doit tout recommencer à chaque nouvelle position de calcul;
\item Cette méthode est itérative, elle coûte du temps de calcul.
\end{itemize}
\end{alertblock}

\end{frame}



\section[Kalman Filter]{Filtrage de Kalman}

\subsection{Présentation}

\begin{frame}{Idée du Filtre de Kalman (KF)}

\begin{block}{Problème d'estimation}
On veut estimer un vecteur $\mbf x$ donc on connaît à une incertitude $\pmb \omega$ près la dynamique, disposant de mesures $\mbf y$ bruités par le vecteur $\pmb \nu$:
\begin{equation}
\begin{cases}
\dot{\mbf x} = \mbf A \mbf x + \mbf B \mbf u + \pmb \omega  \\
\mbf y = \mbf C \mbf x + \pmb \nu.
\end{cases}
\end{equation} 
$\mbf u$ est une éventuelle entrée du système (ex : forces des moteurs sur votre robot).
\end{block}

\begin{alertblock}{D'où vient le KF?}
Si $\pmb \omega$ et $\pmb \nu$ sont des vecteurs gaussiens non-biaisés ($\sim \mathcal{N}(\mbf 0, \Sigma_i)$) alors \textbf{le filtre de Kalman} est calculé comme étant \textbf{le filtre optimal} \footnote{Il minimise la variance des estimés et n'est pas biaisé} dans ce cas.  
\end{alertblock}

\end{frame}

\begin{frame}[allowframebreaks]{Équations de filtrage}
On discrétise l'équation différentielle précédente et on obtient : 
\begin{equation}
\begin{cases}
\mbf x_k = \mbf A_k \mbf x_{k-1} + \mbf B_k \mbf u_k + \pmb \omega_k, \quad \pmb \omega_k \sim \mathcal{N}(\mbf 0, \Sigma_\omega) \\
\mbf y_k = \mbf C_k \mbf x_k + \pmb \nu_k, \quad \pmb \nu_k \sim \mathcal{N}(\mbf 0, \Sigma_\nu)
\end{cases}
\end{equation}
\begin{block}{Étape 0 : Initialisation}
Mon estimé initial est $\hat{\mbf x}_0$ si possible $\mathbb{E}(\mbf x_0)$. \\
Ma variance (incertitude) initiale est $\Sigma_0 = K\Sigma_\omega$, on peut ajuster  $K$ en fonction de la confiance sur ledit estimé $\hat{\mbf x}_0$.
\end{block}
\framebreak
\begin{block}{Étape 1 : Prédiction (j'ai une dynamique... et je l'intègre!)}
La prédiction (pour $k>0$) : \\
$ \mbf x^{pred}_k =  \mbf A_k  \hat{\mbf x}_{k-1} + \mbf B_k \mbf u_k,$ \\
La variance de la prédiction :  \\
$\Sigma^{pred} = \mbf A_k \Sigma_k \mbf A_k^\top +  \Sigma_\omega$.
\end{block} 
\begin{block}{Étape 2 : Gain optimal de Kalman}
Ce gain sert à régler le dilemme \textbf{prédiction/mesure} :  \\
$\mbf G_k = \Sigma^{pred} \mbf C_k^\top (\mbf C_k  \Sigma^{pred} \mbf C_k^\top + \Sigma_\nu)^{-1}$.\\
\textit{Grosso modo} il correspond à $\frac{\text{incertitude prédiction}}{\text{incertitudes mesures + prédiction}}$.
\end{block}
\framebreak
\begin{block}{Étape 3 : Correction des estimés en intégrant les mesures}
On va calculer la différence entre prédiction et mesure : \textbf{l'innovation} (terme de "surprise") : \\
$\mbf i_k = \mbf y_k - \mbf C_k \hat{\mbf x}_k^{pred}$ \\
On obtient l'estimé final : \\ 
$\hat{\mbf x}_k = \hat{\mbf x}^{pred}_k + \mbf G_k \mbf i_k,$ \\
Et sa variance : \\
$\Sigma_k = (\mbf I - \mbf G_k \mbf C_k) \Sigma^{pred}_k .$
\end{block}
On retourne à l'étape 1 ensuite et on effectue l'étape 3 dès que des mesures $\mbf y$ sont disponibles.
\end{frame}



\subsection{Exemple}

\begin{frame}{Trajectoire d'un robot monovariable}
\textbf{Énoncé :}

Supposons un robot sur un axe $x$ avec pour états sa vitesse et sa position $\mbf x = [ x, v]^\top$. Le robot se balade sur $x>0$ et on veut le localiser.

Un dispositif UWB situé en $x=0$ (pour simplifier) nous donne le rayon $\rho = x + \nu$ avec $\nu \sim \mathcal{N}(0,0.1^2)$ (bruit de mesure de $10$ centimètres) toutes les secondes. 

On ne connaît pas la dynamique du robot mais on sait que le robot a une accélération de variance $0.07 \mathrm{m/s^2}$.

La cinématique du robot est donnée par la simple équation $v = \dot{x}$ (on ne connaît malheureusement pas $\mbf u$) :
\[
\dot{\mbf x} = 
\underbrace{
\begin{bmatrix}
0 & 1 \\
0 & 0
\end{bmatrix}
}_{\mbf A}
\mbf x ,
\quad
y = \rho = 
\underbrace{
\begin{bmatrix}
1 & 0
\end{bmatrix}
}_{\mbf C}
\mbf x.
\]
\end{frame}

\begin{frame}{Mise en équation pour le Kalman}
\textbf{Notre modèle est en temps continu...} discrétisons-le :
\[ \mbf A_k (\delta_t) = 
\exp 
\left( \begin{bmatrix}
0 & 1 \\ 
0 & 0 
\end{bmatrix} \delta t \right) 
= \mbf I_2 + \mbf A h_k ~ (\mbf A \text{ est nilpotente!}).
\]
et $\mbf C_k = \mbf C$ car l'observation de $x$ est indépendante du temps (et donc de la discrétisation). \\
\textbf{Les matrices $\Sigma_\nu$ et $\Sigma_\omega$} ?
\begin{itemize}
\item Clairement $\Sigma_\nu = 0.1^2$.
\item Pour $\Sigma_\omega$ il faut discrétiser, le bruit est sur l'accélération la dérivée de $v$ : on suppose donc que $\Sigma_\omega^{continu} = \text{diag}(0,0.07^2).$
En discrétisant : \small
$\Sigma_\omega = \int_{t=0}^{h_k} \mbf A_k(t) \Sigma_\omega^{continu}  \mbf A_k^T(t) dt 
=
0.07^2
\begin{bmatrix}
h_k^3/3 & h_k^2/2 \\
h_k^2/2 & h_k
\end{bmatrix}.
$
\normalsize
\end{itemize}

\end{frame}




\begin{frame}[allowframebreaks]{Simulations}
\begin{figure}
\centering
\includegraphics[width=0.7\textwidth]{fig/kalman.eps}
\caption{Résultats obtenus après filtrage de Kalman}
\end{figure}

\framebreak
\begin{itemize}
\item L'algorithme est récursif donc coûte moins en termes de calcul.
\item Cela fonctionne bien (robustesse aux erreurs d'estimation initiales, convergence assurée malgré le bruit de mesure...) mais...
\item Nous aurions pu estimer l'accélération, dans notre modèle on suppose $a = 0$;
\item Le problème est ici que le cas est en 1D... ce qui rend les \textbf{distances linéaires} si la mesure est toujours strictement positive (arnaque de ma part). Il faut développer une théorie de Kalman non-linéaire!
\end{itemize}

\end{frame}

\section[EKF]{Filtrage de Kalman Étendu}

\subsection{Présentation}

\begin{frame}[allowframebreaks]{Équations de filtrage : linéarisation}
Se plaçant dans un cas \textbf{non-linéaire on a :}
\begin{equation}
\begin{cases}
\dot{\mbf x} = \mbf f(\mbf x, \mbf u) \\
\mbf y = \mbf g(\mbf x)
\end{cases}
\end{equation}
On va réécrire les équations du filtre de Kalman en utilisant en lieu et place des matrices $\mbf A$ et $\mbf C$ des \textbf{Jacobiennes} prises à l'état estimé précédent. Le Filtrage de Kalman Étendu (EKF) est \emph{une linéarisation du Filtrage de Kalman}.
\[ \mbf A_k := \left. \frac{\partial \mbf f_k}{\partial \mbf x}\right\vert_{\hat{\mbf x}_{k-1},{\mbf u}_{k}}, 
\mbf C_k := \left. \frac{\partial \mbf g_k}{\partial \mbf x}\right\vert_{\hat{\mbf x}_{k-1},{\mbf u}_{k}}
\]
ces deux matrices seront à \textbf{mettre} à jour à chacune des itérations de l'EKF.
\framebreak

On reprend strictement la même chose que pour le KF.
\begin{block}{Étape 1 : On utilise $f$}
$ \mbf x^{pred}_k = \hat{\mbf x}_{k-1} + h_k \mbf f (\hat{\mbf x}_{k-1}, \mbf u_k),$ \\
La variance de la prédiction...  :  \\
$\Sigma^{pred} = \mbf A_k \Sigma_k \mbf A_k^\top +  \Sigma_\omega$.
\end{block} 
\begin{block}{Étape 2 : Gain optimal approximé de Kalman}
Il est inchangé :  \\
$\mbf G_k = \Sigma_k^{pred} \mbf C_k^\top (\mbf C_k \Sigma_k^{pred} \mbf C_k^\top + \Sigma_\nu)^{-1}$.\\
\end{block}
\framebreak
\begin{block}{Étape 3 : Correction des estimés en intégrant les mesures}
Vu que nous connaissons $\mbf g$ on peut l'utiliser dans le calcul de \textbf{l'innovation} : \\
$\mbf i_k = \mbf y_k - \mbf g ( \hat{\mbf x}_k^{pred})$ \\
On obtient l'estimé final, inchangé : \\ 
$\hat{\mbf x}_k = \hat{\mbf x}^{pred}_k + \mbf G_k \mbf i_k,$ \\
Et sa variance qui demeure inchangée : \\
$\Sigma_k = (\mbf I - \mbf G_k \mbf C_k) \Sigma^{pred}_k .$
\end{block}
On retourne à l'étape 1 ensuite et on effectue l'étape 3 dès que des mesures $\mbf y$ sont disponibles.

\begin{alertblock}{Remarque}
L'EKF converge de manière robuste en pratique mais n'est plus optimal (le KF l'est ssi le modèle est linéaire).
\end{alertblock}

\end{frame}



\subsection{Intégration dans un robot 2D}

\begin{frame}[allowframebreaks]{Modèle cinématique : fonction $\mbf f$}

On reprend notre cher robot 2D, ses états sont $\mbf x = [x,y,v_t,\theta, \omega]^\top$. Que vaut $\mbf f$ ?
En l'absence d'entrée $\mbf u$ elle vaut :
\[ \dot{\mbf x} = \mbf f(\mbf x) = 
\begin{bmatrix}
{\color{red} v_t\cos(\theta)} \\
{\color{red} v_t\sin(\theta)} \\
0 \\
\dot{\theta} \\
0
\end{bmatrix}
\]
la partie non linéaire est en {\color{red} rouge} c'est une conversion entre la vitesse tangentielle $v_t$ et les deux vitesses axiales $\dot{x}$ et $\dot{y}$.
\\
Les zéros proviennent du fait que nous avons pas de dynamique ici ($\mbf u$ n'est pas prise en compte) sinon il y aurait des équations différentielles. 

La jacobienne vaut :
\[\mbf A(\mbf x)
=
\begin{bmatrix}
0 & 0 & \cos(\theta) & -v_t \sin(\theta) & 0 \\
0 & 0 & \sin(\theta) & v_t \cos(\theta) & 0 \\
0 & 0 & 0 & 0 & 0 \\
0 & 0 & 0 & 0 & 1 \\
0 & 0 & 0 & 0 & 0 
\end{bmatrix}
,\]
pour discrétiser la dynamique suivant un pas $h_k$ on utilisera simplement Euler :
\[ \mbf x_k \approx \mbf x_{k-1} + h_{k} \dot{\mbf x}_{k} \approx \mbf x_{k-1} + h_k \mbf A(\mbf x_{k-1}) \mbf x_{k-1} \]
ce qui fait que la matrice $\mbf A_k$ implémentée pour la prédiction vaudra:
\[ \mbf x_k^{pred} \approx  \mbf A_{k} \hat{\mbf x}_{k-1}, 
\quad 
\mbf A_k = \mbf I_5 + h_{k} \mbf A(\hat{\mbf x}_{k-1}) \]
\end{frame}


\begin{frame}[allowframebreaks]{Capteurs... : fonctions $\mbf g_i$}
En général pour un robot au sol, les IMU sont de mauvaises qualité : il ne faut pas utiliser l'accéléromètre.

On utilise 
\begin{enumerate}
\item Un \textbf{magnétomètre} sur un seul de ses axes qui nous donnera $\tilde{\theta}$;
\item  L'odométrie \textbf{des encodeurs des roues} qui nous donnera $\tilde{\omega}$ et $\tilde{v}_t$;
\begin{itemize}
\item Si $R_i$ sont les rayons des roues droites et gauche et $\omega_i$ leur vitesse de rotation;
\item Alors \cite{cano_synchronisation_2019} on a (voir schéma slide 7) : \\
$v_t = \frac{\omega_g R_g + \omega R_d}{2} \text{ et } \omega = \frac{\omega_{g}R_g - \omega_{d}R_d}{L}$.
\end{itemize}
\item Un \textbf{système de balises UWB} qui nous donnera un estimé de distance $\rho_i$, distance d'un ancre $A_i$ à position connue $\mbf a_i$ au robot.
\end{enumerate}

\framebreak

Il existe trois sortes de mesures, \textbf{la fonction} $\mbf g$ peut prendre trois formes différentes.

Respectivement : 
\begin{enumerate}
\item $g_1 (\mbf x) = \theta \Rightarrow \mbf C = 
\begin{bmatrix}
0 & 0 & 0 & 1 & 0
\end{bmatrix}
$
\item $\mbf g_2 (\mbf x) = 
\begin{bmatrix}
\omega_g \\
\omega_d
\end{bmatrix} \Rightarrow \mbf C = 
\begin{bmatrix}
0 & 0 & R_g/2 & 0 & R_g/L \\
0 & 0 & R_d/2 & 0 &-R_d/L
\end{bmatrix}
$
\item Pour l'UWB, avec les ancres $A_i = (a_i^x,a_i^y)$, on a : \\
$g_3(\mbf x) = \rho_i = \sqrt{(x-a_i^x)+(y-a_i^y})$
\[ \Rightarrow \mbf C(\mbf x) = 
\begin{bmatrix}
\frac{x-a_i^x}{\rho_i} & \frac{y-a_i^y}{\rho_i} & 0 & 0 & 0
\end{bmatrix}.
\]
\end{enumerate}
ces trois fonctions et leur jacobienne seront appelées en fonction des mesures disponibles. Les $\Sigma_\nu$ changent en fonction des caractéristiques des capteurs (voir code).

\end{frame}

\subsection{Simulations}

\begin{frame}[allowframebreaks]{Simulations}
\begin{figure}
\centering
\includegraphics[width=0.8\columnwidth]{fig/ekf_trajectory.eps}
\caption{Trajectoire traquée et estimés.}
\end{figure}

\begin{figure}
\centering
\includegraphics[width=0.8\columnwidth]{fig/ekf_states.eps}
\vspace{-1cm}
\caption{Chronogramme des états.}
\end{figure}
\end{frame}

\section*{Lectures}

\begin{frame}{Lectures}

\begin{itemize}
\item À lire impérativement (bouquin très abordable théoriquement avec des \textbf{codes et des exemples}) : \cite{kovvali_introduction_2013};
\item Chui et Chen, bouquin de référence sur le KF : \cite{chui_kalman_1999};
\item Le mémo sur les robots 2D sur mon site sur le déploiement des robots \url{http://justincano.com} (la cinématique y est expliquée);
\item Intégration sur un robot 2D avec formulation dynamique, mon mémoire, Chapitre IV, bien plus rigoureux : \cite{cano_synchronisation_2019} ;
\item Codes en Python et Matlab de démonstration, mon repo : \url{https://gitlab.com/nitraced/ekf_uwb_demos} un autre repo existe en C sur mon compte gitlab (nitraced).
\end{itemize}
\end{frame}

\begin{frame}{Bibliographie}
\bibliographystyle{apalike}
\bibliography{main}
\end{frame}






\end{document}