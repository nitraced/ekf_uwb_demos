clear all 
close all
global ref mes
%% Parameters

% References (aka anchors)
ref = [0 1; 1 0; 1 1;-1 1]';
% True position of the robot
robot = [0,0]';
% Noise 
sigma_noise = 0.1;
%% Measurement simulation
% Distances 
n = length(ref);
D = zeros(n,1);

for i=1:n
     D(i)=norm(ref(:,i)-robot);
end

% Noise vector
nu = randn(n,n)*sigma_noise;
mes= D + nu;    


%% Least squares
robot_hat = lsqnonlin(@distres,[0,0]');

%% Plot
scatter(ref(1,:),ref(2,:));
hold on
grid minor
scatter(robot(1),robot(2));
scatter(robot_hat(1),robot_hat(2));

t = linspace(0,2*pi,100);
for i=1:n
   plot(ref(1,i)+mes(i)*cos(t),ref(2,i)+mes(i)*sin(t),':')
end

xlabel('X axis [m]')
ylabel('Y axis [m]')
legend('Reference anchors','Actual position','Estimated position','Measurements')


function  r = distres(x)
    global ref mes
    r = zeros(length(ref),1);
    for i=1:length(ref)
        r(i) = norm(x - ref(:,i))-mes(i);
    end
end