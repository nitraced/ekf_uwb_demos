% Extended Kalman Filter example for UGV Navigation
clear all
close all
%% Robot motion simulator : circle
N = 1000;
T = 40;
% Symbolic generator
syms t real
% Circle
omega = 2*pi/T;
r = 1;
p = [r*cos(omega*t); r*sin(omega*t)];
% Velocity
v = jacobian(p,t);
% Lazy way to compute good jacobian
pose = matlabFunction(p);
velo = matlabFunction(v);
% Numerical generation
t = linspace(1,T,N);
% Circle angle + 90 degrees = robot heading
theta = t*omega + pi/2;
% Angular Velocity = omega
omegav = omega*ones(size(t));
% Simulated states
vt =  sqrt(sum(velo(t).^2));
X = [
    pose(t); % x,y
    vt; % Vt
    theta;
    omegav
    ];

%% Simulation of the measurement
% Noises std
std_odometry = 0.01;
std_magneto = deg2rad(5); % 5 degrees
std_uwb = 0.1;
% Length of the chassis + radius of the wheels
L = 0.4;
R = 0.1;
std_odometry_translation = std_odometry*R/sqrt(2);
std_odometry_rotation = std_odometry*sqrt(2)*R/L;
% Simulation
odometry_translation = vt + randn(1,N)*std_odometry_translation;
odometry_rotation = omegav+randn(1,N)*std_odometry_rotation;
magneto = theta + randn(1,N)*std_magneto;
% For UWB it's more complicated... we have N stations let's say 3
a1 = [-2; 0];
a2 = [2;-2];
a3 = [2;2];
a = [a1 a2 a3];
% We stack the measurements into this vector
uwb = [
    sqrt(sum((pose(t)-a1).^2)) + randn(1,N)*std_uwb;
    sqrt(sum((pose(t)-a2).^2)) + randn(1,N)*std_uwb;
    sqrt(sum((pose(t)-a3).^2)) + randn(1,N)*std_uwb;
    ];

% Measurement order 1,2 then 3 (you can modify to see what is happening)
% 1 : odometry
% 2 : magnetometer
% 3 : uwb (we increment a counter to know wich anchor is used)
mes_choosen = 1;
uwb_choosen = 1;
%% EKF

% delta t is constant
dt = mean(diff(t));

% This is a weigh for the model confidence
sigma_omega = dt*diag([0.01,0.01,0.2,0.01,0.2]).^2;
% Diagonal matrix to simplify ... EKF is not exact indeed

% Initial states
sigma = sigma_omega*10; % We do not know where we are
% Vector ready
xhat = zeros(5,N);
xhat0 = [0 0 0 0 0]';
xhat(:,1)=xhat0;

for i=2:N
    mes_choosen = mod(i,3)+1;
    % Prediction
    % f is evaluated there
    x = xhat(1,i-1);
    y = xhat(2,i-1);
    vt = xhat(3,i-1);
    theta = xhat(4,i-1);
    omega = xhat(5,i-1);
    f = [vt*cos(theta);vt*sin(theta);0;omega;0];
    % Prediction equation
    x_pred = xhat(:,i-1)+dt*f;
    % The jacobian...
    A = eye(5)+ dt*[
        0 0 cos(theta) -vt*sin(theta) 0;
        0 0 sin(theta) vt*cos(theta) 0;
        0 0 0 0 0;
        0 0 0 0 1;
        0 0 0 0 0];
    % Prediction variance
    sigma_pred = A*sigma*A'+sigma_omega;
    % Measurement handle and prediction
    switch mes_choosen
        case 1
            % Odometry
            y = [odometry_translation(i);
                odometry_rotation(i)];
            % Mes variance
            sigma_nu = diag([std_odometry_translation^2,std_odometry_rotation^2]);
            % Jacobian (we have yet done the kinematic conversion (the
            % noise is mapped with respect to this so the coeffs are 1)
            C = [0 0 1 0 0;
                 0 0 0 0 1];
            % Prediction
            gxpred = [x_pred(3);x_pred(5)];
        case 2
            % Magnetometer
            y = magneto(i);
            % Mes variance
            sigma_nu = std_magneto^2;
            % Jacobian
            C = [0 0 0 1 0];
            % Prediction
            gxpred = x_pred(4);
        case 3
            % UWB
            y = uwb(uwb_choosen,i);
            % Mes variance
            sigma_nu = std_uwb^2;
            % Jacobian
            C = [x-a(1,uwb_choosen),y-a(2,uwb_choosen),0,0,0]/norm([x;y]-a(:,uwb_choosen));
            % Prediction
            gxpred = norm([x_pred(1);x_pred(2)]-a(:,uwb_choosen));
            if uwb_choosen<3
               % Prepares another station for the next time
               uwb_choosen = uwb_choosen +1; 
            else
               uwb_choosen = 1;
            end
    end
    % Kalman gain equation
    G = sigma_pred*C'*(C*sigma_pred*C' + sigma_nu)^-1;
    % Update equation
    xhat(:,i) = x_pred + G*(y-gxpred);
    % Update variance
    sigma = (eye(5)-G*C)*sigma_pred;
end

%% Display
plot(X(1,:),X(2,:),'--');
hold on 
grid on
plot(xhat(1,:),xhat(2,:))
plot(a(1,:),a(2,:),'d')
legend({'Actual Trajectory','Estimates','Anchors'},'Location','best')


%%
figure
names = {'x Position [m]','y Position [m]','V_t velocity [m/s]','\theta angle [rad]','\omega angular velocity [rad/s]'};
for i=1:5
   subplot(5,1,i)
   plot(t,X(i,:))
   hold on
   grid on
   plot(t,xhat(i,:))
   title(names(i));
   legend({'Ground truth','Estimates'},'Location','best')
end








