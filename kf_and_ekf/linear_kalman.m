%% Linear Kalman example
clear all
close all
%% Robot simulation
T = 10;
N = 500;
t = linspace(0,T,N);
delta_t = t(2)-t(1);
% 500 simulation points
% Acceleration steps
a = 0.07*cos(t*2*pi/T);
% Velocity 
v = cumsum(a*delta_t);
% Position
x = cumsum(v*delta_t)+1.5;

% Measurment simulation
nu = randn(1,N)*0.1;
rho = x + nu;

%% Kalman Filter
% Note : the time step is constant for implementation, think that this
% might change...
A = [1 delta_t; 0 1];
C = [1 0];
xhat = zeros(2,N);
xhat(:,1) = [0 0]'; % We dont know at first... (robustness test)
Sigma_nu=0.1;  
Sigma_omega = 0.07*[delta_t^3/3 delta_t^2/2; delta_t^2/2 delta_t];
Sigma= 10*Sigma_omega; % We supose that...
for i=2:N
    % Prediction
    x_guess = A*xhat(:,i-1);
    Sigma_guess = A*Sigma*A'+Sigma_omega;
    % Kalman Gain computation
    G = Sigma_guess*C'*(C*Sigma_guess*C'+Sigma_nu)^-1;
    % Innovation
    in = rho(i)-C*x_guess;
    % Update
    xhat(:,i)=x_guess+G*in;
    Sigma = (eye(2)-G*C)*Sigma_guess;
end

%% Plot trajectories
figure
subplot(3,1,1)
% Acceleration
plot(t,a,'--')
grid on
legend('Acceleration','Location','best')
% Velocity
subplot(3,1,2)
plot(t,v,'--')
hold on
grid on
plot(t,xhat(2,:))
legend({'Velocity','Estimate'},'Location','best')
% Position
subplot(3,1,3)
plot(t,x,'--')
hold on
grid on
plot(t,xhat(1,:))
legend({'Position','Estimate'},'Location','best')

