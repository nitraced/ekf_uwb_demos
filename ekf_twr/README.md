Created by Justin CANO, August 2018

This is an EKF formulation for UWB ranging purposes, technical notes will be added soon in this repo.

Two python con are present, one for the simulation and distance calculum. And one the main script containing the EKF and plotting features.

You will also find some figs showing the usage of our KF.

Important : before using, you have to install both numpy and matplotlib python libraries.

