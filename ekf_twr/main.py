import sim_robot as s
import numpy as np
import matplotlib.pyplot as plt

# Object to store our estimation results
class estimation:
	def __init__(self,N_points):
		self.N = N_points
		self.x = [0]*N_points
		self.y = [0]*N_points
		self.vx = [0]*N_points
		self.vy = [0]*N_points
		
measure = []
indices_base = []
# First we do a simulation of the robot
sim = s.xy_simulation()

est = estimation(len(sim.measures))

# Initial Conditions
P = np.eye(4)
X = np.matrix([[0.0],[0.0],[0.0],[0.0]])
# Tuning
x_psd = 0.1
y_psd = 0.1
r_std = 0.1

R = r_std
# We are now able to do an estimation of the position
# For each mesaures proceed to estimation 
# MAIN ESTIMATION LOOP
for i in range(est.N):
	# First we need to measure timestep
	if i==1:
		# We do a litle guess
		dt = 0.1
	else:
		# We truely measure the timstep
		dt = sim.tmeasures[i]-sim.tmeasures[i-1]
	# Matrix A and matrix Q updates 
	# Dynamic process
	A = np.matrix([[1.0,dt,0.0,0.0],[0.0,1.0,0.0,0.0],[0.0,0.0,1.0,dt],[0.0,0.0,0.0,1.0]])
	# Model or state-space noise
	Q = np.matrix([[x_psd*dt**3/3.0, x_psd*dt**2/2.0, 0.0, 0.0],[x_psd*dt**2/2.0, x_psd*dt, 0.0, 0.0],[0.0, 0.0,y_psd*dt**3/3.0, y_psd*dt**2/2.0],[0.0, 0.0, y_psd*dt**2/2.0, y_psd*dt]])
	# EKF PREDICTION STEPS
	# Prediction
	X_pred = A*X
	# Prediction variance
	P_pred = A*P*A.transpose() + Q
	# MEASUREMENT FORECASTS AND JACOBIAN
	# Coordinate extraction
	x_pr = X_pred[0,0]
	y_pr = X_pred[2,0]
	x_base = sim.bases[sim.measuring_anchor[i]][0]
	y_base = sim.bases[sim.measuring_anchor[i]][1]
	# Distance as foreseen
	d = s.disttobase([x_pr, y_pr, 0.75],sim.measuring_anchor[i])
	# Jacobian computation
	J = np.matrix([(x_pr-x_base)/d, 0.0, (y_pr-y_base)/d, 0])
	# EKF INNOVATION COMPUTATION
	innov = sim.measures[i]-d
	cov = J*P_pred*J.transpose()+R
	inv_cov = np.matrix([1/cov[0,0]])
	# KALMAN GAIN COMPUTATION
	K = P_pred*J.transpose()*inv_cov
	# UPDATE STEPS
	# estimation step
	X = X_pred + K*innov
	# estimate covariance
	P = (np.eye(4)-K*J)*P_pred
	# storage
	est.x[i] = X[0,0]
	est.vx[i] = X[1,0]
	est.y[i] = X[2,0]
	est.vy[i] = X[3,0]

# Plots
plt.figure(1)
plt.subplot(4,1,1)
plt.plot(sim.t,sim.x,label='true')
plt.plot(sim.tmeasures,est.x,'.',label='EKF')
plt.legend()
plt.ylabel('$x(t) [m]$')
plt.grid(True)

plt.subplot(4,1,2)
plt.plot(sim.t,sim.y,label='true')
plt.plot(sim.tmeasures,est.y,'.',label='EKF')
#plt.legend()
plt.ylabel('$y(t) [m]$')
plt.grid(True)

plt.subplot(4,1,3)
plt.plot(sim.t,sim.vx,label='true')
plt.plot(sim.tmeasures,est.vx,'.',label='EKF')
#plt.legend()
plt.ylabel('$v_x(t) [m/s]$')
plt.grid(True)

plt.subplot(4,1,4)
plt.plot(sim.t,sim.vy,label='true')
plt.plot(sim.tmeasures,est.vy,'.',label='EKF')
#plt.legend()
plt.ylabel('$v_y(t) [m/s]$')
plt.xlabel('time [s]')
plt.grid(True)


plt.figure()
plt.plot(sim.x,sim.y,'--',label='True trajectory')
plt.plot(est.x,est.y,'.',label='EKF Estimation')
for i in range(4):
	plt.plot(sim.bases[i][0],sim.bases[i][1],"p")
plt.xlabel('X axis [m]')
plt.ylabel('Y axis [m]')
plt.title('2D trajectory with anchors positions')

plt.legend()

plt.grid(True)
plt.show()
