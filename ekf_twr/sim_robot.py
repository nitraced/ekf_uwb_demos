import numpy as np
import math as m
import matplotlib.pyplot as plt
import matplotlib.mlab as mlab

# Location of the bases
bases = [[-3, -3, 1.5],[3, -3, 1.5],[3, 3, 1.5],[-3,3, 1.5]]
# Variances of the ranges
std_ranges = 0.1

# Class to store simulation results 
class Robotsim:
	def __init__(self,N_points):
		# memory allocation
		self.t = [0]*N_points
		self.x= [0]*N_points
		self.y = [0]*N_points
		self.vx = [0]*N_points
		self.vy = [0]*N_points
		self.measures = []
		self.tmeasures = []
		self.measuring_anchor = []
		self.bases = bases

# Returns the distances between a base station and a treedimensional vector
def disttobase(x,i):
	# Euclidian distance
	sq = 0
	for j in range(3):
		# Sum of the squares
		sq += (bases[i][j]-x[j])**2
	# DO NOT forget sqrt ;) 
	return m.sqrt(sq)

# Returns the distance plus additive noise 
def pseudorange(xtrue,i):
	# We add the White Gaussian Noise Nu at the end of this 
	nu = np.random.normal()*std_ranges;
	# the measurement is the true value plus the noise
	return nu + disttobase(xtrue,i)

def monteCarloDebugRanges():
	# To see our normal distribution of our simulation (not used in final script but usefull for debug and learning ;) )
	s = [0]*10000
	for i in range(10000):
		s[i] = pseudorange([1,1,1],1)
	N, bins, patches = plt.hist(s, 50, normed = 1, facecolor='green',alpha=0.75)
	plt.xlabel('Ranges measured')
	plt.ylabel('Probabilities')
	plt.grid(True)
	plt.show()
	print('std = '+ str(np.std(s)) + ' mean = ' + str(np.mean(s)))

def xy_simulation():

## TIME SETTINGS 
	# Time step is constant between two steps of our simulation let say that our simulation is done with 
	# a frequence of 20kHz
	f_simulation = 20.0
	# Our trajectory is lasting 30 seconds
	duration = 60.0
	# Number of simulated points
	N_points = int(f_simulation*duration)
	# Frequence of our update between two anchors 
	f_measurement = 5.0
	# Number of simulated points between two measurement (round)
	N_sim_between_mes = int(f_simulation/f_measurement)
	
## TRAJECTORY SETTINGS
	# The trajectory is elliptic
	# X amplitude 
	ax = 1.0
	# Y amplitude
	ay = 0.8
	# Vehicle tangential speed (assumed constant) [m/s]
	v_tangential = 0.1
	# Note : rotation speed is depending of course of the distance with respect to the center of the ellipse
	
	# Simobject creation
	mysim = Robotsim(N_points)
	# Rotation rate is initialized at 0
	omega= 0.1
	measuring_anchor = 0
	z_robot = 0.75
	# Simulation loop
	for i in range(N_points):
		# Time incrementation
		if i>1:
			mysim.t[i] = mysim.t[i-1] + 1/f_simulation
		# Trajectory 
		mysim.x[i] = ax*np.cos(omega*mysim.t[i])
		mysim.y[i] = ay*np.sin(omega*mysim.t[i])
		# Velocities
		mysim.vx[i] = -ax*omega*m.sin(omega*mysim.t[i])
		mysim.vy[i] = ay*omega*m.cos(omega*mysim.t[i])
		# Omega update (velcity normalized by the euclidian distance to the center)
		#dist = np.sqrt(mysim.x[i]**2 + mysim.y[i]**2)
		#omega = v_tangential/dist
		# Measurements simulation 
		if i%N_sim_between_mes == 0:
			mysim.tmeasures.append(mysim.t[i]);
			mysim.measures.append(pseudorange([mysim.x[i],mysim.y[i],z_robot],measuring_anchor))
			mysim.measuring_anchor.append(measuring_anchor)
			if measuring_anchor == 3:
				measuring_anchor = 0
			else:
				measuring_anchor += 1
	# Structure exportation
	return mysim
