% Close all
% Simulations parameters.
% Accelerometer drift (dead reckogning)
sampling_time = 1/1000; % période d'échantillonnage
psd_accelerometer = 1e-4; % 1 ppm/s (pas beaucoup)
bias = ones(2,1)*1e-3;
time_of_simulation= 10;
omega_rotation = time_of_simulation/(2*pi); % On veut faire un demi tour seulement

syms t real
x = cos(t/omega_rotation);
y = sin(t/omega_rotation);
% Position
p = [x,y]';
matlabFunction(p,'File','circle_p')
% Condition initiale pour l'integrateur
p0 = circle_p(0);
% Vitesse
v = jacobian(p,t);
matlabFunction(v,'File','circle_v')
v0 = circle_v(0);
% Acceleration
a = jacobian(v,t);
matlabFunction(a,'File','circle_a')

sim('dr_circle.slx')
%%
% 3d plot
close all
title('Positions estimated vs true')
plot(pose.signals(1).values(:,1),pose.signals(1).values(:,2),'--');
hold on
plot(pose.signals(2).values(:,1),pose.signals(2).values(:,2),'.');
legend({'Estimation','Position'},'Location','best')
grid

