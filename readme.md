# Robot navigation repository

##### Justin Cano 

Ph.D. Student, Polytechnique Montréal & ISAE Supaéro

Can be contacted by using his google ID : cano.justin 

### Contents

This repository contains demo code for robot localization in Matlab and Python.

Folders description :

- `doc/` <u>Contains technical notes for the repo</u>
  - `ekf_uwb_doc.pdf` : (to be continued) explains briefly Kalman filtering to perform robot localization aided with UWB (see [1,2]) measurements. I work on this technical note in English this will be soon released depending on my free time.
  - `localisation_robots.pdf` (in French) explains all the algorithms there (done for a Crash course at Centrale Marseille).
- `dead_reckoning/` : Matlab/Simulink code showing the bad effects of kinematics integration without any external measurements.
- `ekf_twr/` : Python code to perform Extended Kalman Filtering for localization of an embedded transceiver with the more generalist model : **acceleration seen as a Brownian motion** aka constant velocity model.
- `kf_and_ekf/` : Two Matlab Scripts. One to show linear KF with range measurement in simplistic 1D case, one to show EKF with kinematic model of the UGV (2D Robot).
  - The UGV model is explained in the slides `localisation_robots.pdf` : it's a simple non-holonomic constrained kinematic model.
  - The sensor used are **compass**, **wheels odometry** and **UWB**. 

### Remark

Another repo with `C` implementation [1] (and `Matlab` simulation) and an exhaustive documentation can be found at : https://gitlab.com/nitraced/ranging_ekf

### Bibliography

[1] J. Cano, S. Chidami, et J. L. Ny, « A Kalman Filter-Based Algorithm for Simultaneous Time Synchronization and Localization in UWB Networks », in *2019 International Conference on Robotics and Automation (ICRA)*, 2019, p. 1431‑1437, doi: [10.1109/ICRA.2019.8794180](https://doi.org/10.1109/ICRA.2019.8794180).

[2] J. Cano, « Synchronisation et positionnement simultanés d’un réseau ultra-large bande et applications en robotique mobile », Master’s Thesis, Polytechnique Montréal, Montreal, QC, Canada, 2019. Available on my personal website : http://justincano.com/recherche.html